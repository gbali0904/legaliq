package com.legaliq.android.app.application;

/**
 * Created by Gunjan on 10-07-2017.
 */

import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.legaliq.android.app.R;
import com.legaliq.android.app.rest.ApiInterface;
import com.legaliq.android.app.utility.Constants;
import com.rey.material.widget.ProgressView;

import io.fabric.sdk.android.Fabric;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Scheduler;
import rx.schedulers.Schedulers;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


/**
 * Created by gunjan on 2018/09/10.
 */

public class AppController extends Application {

    public static final String TAG = AppController.class
            .getSimpleName();
    private static SharedPreferences sharedPreferences;
    private static AppController instance;

    private static final String PASSWORD_PATTERN =
            "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$";

    private static final String MOBILE_PATTERN = "[0-9]{10}";

    private static final String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private static AppController mInstance;
    private ProgressDialog progressDialog;
    private SharedPreferences pref;
    private Gson gson;
    private ProgressDialog progressDialogImages;
    private Dialog mProgressDlg;

    private Scheduler defaultSubscribeScheduler;
    private ProgressView pv_circular_colors;

    public static synchronized AppController getInstance() {

        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Raleway-Medium.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(AppController.this);
        mInstance = this;
    }

    public SharedPreferences getPreference() {
        if (pref == null)
            pref = PreferenceManager.getDefaultSharedPreferences(AppController.this);
        return pref;
    }
    /**
     * This method is used to get instance of Default Shared preference editor
     * which can be used throughout the application.
     *
     * @return singleton instance of default shared preference eidtor
     */
    public SharedPreferences.Editor getPreferenceEditor() {
        if (pref == null)
            pref = PreferenceManager.getDefaultSharedPreferences(AppController.this);
        return pref.edit();
    }

    public static void savePreferences(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }
    public static String getPreferences(String key, String val) {
        String value = sharedPreferences.getString(key, val);
        return value;
    }
    public Gson getGson() {
        if (gson == null)
            gson = new Gson();
        return gson;
    }
    public ApiInterface getRetrofitServices() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.MINUTES)
                .readTimeout(10, TimeUnit.MINUTES)
                .writeTimeout(10, TimeUnit.MINUTES)
                .addInterceptor(logging);
        httpClient.addInterceptor(logging);  // <-- this is the important line!
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(httpClient.build())
                .build();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        return apiInterface;
    }

    public Scheduler defaultSubscribeScheduler() {
        if (defaultSubscribeScheduler == null) {
            defaultSubscribeScheduler = Schedulers.io();
        }
        return defaultSubscribeScheduler;
    }

    public void showProgressDialog(Context context) {
        mProgressDlg = new Dialog(context);
        mProgressDlg.setCancelable(false);
        mProgressDlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(mProgressDlg.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        mProgressDlg.getWindow().setAttributes(lp);
        mProgressDlg.setContentView(R.layout.progress_dialog);
        pv_circular_colors = (ProgressView)mProgressDlg.findViewById(R.id.progress_pv_circular_colors);
        pv_circular_colors.start();
        mProgressDlg.show();

    }
    public void hideProgressDialog() {
        if (mProgressDlg != null) {
            if (mProgressDlg.isShowing())
                mProgressDlg.dismiss();
        }

    }


    public Boolean passwordValidator(String password){
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }


    public Boolean mobileNoValidator(String mobile_no){
        Pattern pattern = Pattern.compile(MOBILE_PATTERN);
        Matcher matcher = pattern.matcher(mobile_no);
        return matcher.matches();
    }


    public Boolean emailValidator(String email){
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public Bundle firbaseAnalytics(String id, String name, FirebaseAnalytics mFirebaseAnalytics)
    {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        return bundle ;
    }
    public boolean containsURL(String content){
        String REGEX = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
        Pattern p = Pattern.compile(REGEX, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(content);
        if(m.find()) {
            return true;
        }

        return false;
    }

    public static String getMonthName(int monthIndex) {
        //since this is zero based, 11 = December
        if (monthIndex < 0 || monthIndex > 11 ) {
            throw new IllegalArgumentException(monthIndex + " is not a valid month index.");
        }
        return new DateFormatSymbols().getMonths()[monthIndex-1].toString();
    }
    public Date getMeYesterday(){
        return new Date(System.currentTimeMillis()-24*60*60*1000);
    }
    public Date getMeTomorrow(){
        return new Date(System.currentTimeMillis()+24*60*60*1000);
    }
    public Date getMeToday(){
        return new Date(System.currentTimeMillis());
    }

    public static String getDate(Date milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(milliSeconds);
        Date parse=null;
        try {
            parse = formatter1.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formatter1.format(parse);
    }


}
