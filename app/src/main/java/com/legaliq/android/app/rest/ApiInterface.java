package com.legaliq.android.app.rest;


import com.legaliq.android.app.login.model.ModelForForgotPassword;
import com.legaliq.android.app.login.model.ModelForLogin;
import com.legaliq.android.app.setting.changePassword.model.ModelForChangePassword;

import java.util.Map;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by gunjan on 2018/09/10.
 */

public interface ApiInterface {

    @FormUrlEncoded
    @POST("User_Login")
    Observable<ModelForLogin> login(@Field("userId") String userId, @Field("password") String password,
            @Field("deviceToken") String deviceToken, @Field("deviceModel") String deviceModel,
            @Field("osType") String osType, @Field("osVersion") String osVersion, @Field("appVersion") String appVersion);

    @FormUrlEncoded
    @POST("")
    Observable<ModelForForgotPassword> forgotPassword(@Field("mobile_number") String mobile_number);

    @FormUrlEncoded
    @POST("")
    Observable<ModelForChangePassword> changeFirstTimeLoginPassword(@Header("Authorization") String authorization,
                                                                    @Field("password") String password);

    @FormUrlEncoded
    @POST("")
    Observable<ModelForLogin> changePassword(@Field("user_id")String user_id,
                                             @Field("user_pass") String user_pass,
                                             @Field("user_newPass") String user_newPass);

}
