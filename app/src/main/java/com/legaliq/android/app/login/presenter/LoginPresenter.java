package com.legaliq.android.app.login.presenter;

/**
 * Created by gunjan on 2018/09/10.
 */
public interface LoginPresenter {
	void doLogin(String name, String passwd);
	void setProgressBarVisiblity(int visiblity);

    void doforgotpassword(String mobile_number);
}
