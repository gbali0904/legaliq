package com.legaliq.android.app.login.view;


import com.legaliq.android.app.login.model.ModelForForgotPassword;
import com.legaliq.android.app.login.model.ModelForLogin;

/**
 * Created by gunjan on 2018/09/10.
 */
public interface LoginView {
	//public void onLoginResult(Boolean result, ModelForLogin code);
	public void onSetProgressBarVisibility(int visibility);

	/**
	 * Login success.
	 * @param modelForLogin
	 */
	void loginSuccess(ModelForLogin modelForLogin);

	/**
	 * Login error.
	 * @param message
	 */
	void loginServerError(String message);


    void forgotPasswordSuccess(ModelForForgotPassword modelForForgotPassword);
}
