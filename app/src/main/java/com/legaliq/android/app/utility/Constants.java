package com.legaliq.android.app.utility;
/**
 * Created by gunjan on 2018/09/10.
 */

public class Constants {
    public static final String VERSION_NAME = "version_name";
    public static final String GCM_ID = "gcm_id";
    public static final String FIREBASE_URL ="https://fcm.googleapis.com/fcm/send" ;

    // public static String BASE_URL="http://3b19b9b8.ngrok.io/PropIQ/";
    public static String BASE_URL="http://db560343.ngrok.io/LegalIQ/";
    public static String DEVICE_TYPE="Android";
    public static String DEVICE_MODEL="device_model";
    public static String OS_VERSION="os_version";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    public static final String SHARED_PREF = "ah_firebase";

}
