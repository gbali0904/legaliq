package com.legaliq.android.app.login.model;

import java.util.List;

/**
 * Created by gunjan on 2018/09/10.
 */
public class ModelForLogin {

    /**
     * message : List Successfully Featched
     * users : {"id":7,"first_name":"Gunjan","last_name":"Bali","password":"$2a$10$/dkDHZ490zC2S/cMZ/UOX.9z45W8r7.4kVJ1RhRTI9UgnVG3Bbq4i","email":"gunjan.bali@covetlo.com","country_code":"+44","country_name":"IN","mobile_no":"9876543212","image":"hello.jpg","first_login":0,"createc_at":"2018-10-04 10:21:25.0","updated_at":"2018-10-04 10:28:24.0","roles":[{"id":1,"role":"ROLE_ADMIN"}],"deviceInfo":{"id":5,"os_type":"android","device_token":"797EAA8E-59FE-4844-9C01-08BEE15B137A","device_model":"Pixel","os_version":"8.1.0","app_version":"1.0","createc_at":"2018-10-04 10:21:25.0","updated_at":"2018-10-04 10:21:25.0"}}
     * status : true
     * responseCode : 200
     */

    private String message;
    private UsersBean users;
    private boolean status;
    private int responseCode;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UsersBean getUsers() {
        return users;
    }

    public void setUsers(UsersBean users) {
        this.users = users;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public static class UsersBean {
        /**
         * id : 7
         * first_name : Gunjan
         * last_name : Bali
         * password : $2a$10$/dkDHZ490zC2S/cMZ/UOX.9z45W8r7.4kVJ1RhRTI9UgnVG3Bbq4i
         * email : gunjan.bali@covetlo.com
         * country_code : +44
         * country_name : IN
         * mobile_no : 9876543212
         * image : hello.jpg
         * first_login : 0
         * createc_at : 2018-10-04 10:21:25.0
         * updated_at : 2018-10-04 10:28:24.0
         * roles : [{"id":1,"role":"ROLE_ADMIN"}]
         * deviceInfo : {"id":5,"os_type":"android","device_token":"797EAA8E-59FE-4844-9C01-08BEE15B137A","device_model":"Pixel","os_version":"8.1.0","app_version":"1.0","createc_at":"2018-10-04 10:21:25.0","updated_at":"2018-10-04 10:21:25.0"}
         */

        private int id;
        private String first_name;
        private String last_name;
        private String password;
        private String email;
        private String country_code;
        private String country_name;
        private String mobile_no;
        private String image;
        private int first_login;
        private String createc_at;
        private String updated_at;
        private DeviceInfoBean deviceInfo;
        private List<RolesBean> roles;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getCountry_code() {
            return country_code;
        }

        public void setCountry_code(String country_code) {
            this.country_code = country_code;
        }

        public String getCountry_name() {
            return country_name;
        }

        public void setCountry_name(String country_name) {
            this.country_name = country_name;
        }

        public String getMobile_no() {
            return mobile_no;
        }

        public void setMobile_no(String mobile_no) {
            this.mobile_no = mobile_no;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public int getFirst_login() {
            return first_login;
        }

        public void setFirst_login(int first_login) {
            this.first_login = first_login;
        }

        public String getCreatec_at() {
            return createc_at;
        }

        public void setCreatec_at(String createc_at) {
            this.createc_at = createc_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public DeviceInfoBean getDeviceInfo() {
            return deviceInfo;
        }

        public void setDeviceInfo(DeviceInfoBean deviceInfo) {
            this.deviceInfo = deviceInfo;
        }

        public List<RolesBean> getRoles() {
            return roles;
        }

        public void setRoles(List<RolesBean> roles) {
            this.roles = roles;
        }

        public static class DeviceInfoBean {
            /**
             * id : 5
             * os_type : android
             * device_token : 797EAA8E-59FE-4844-9C01-08BEE15B137A
             * device_model : Pixel
             * os_version : 8.1.0
             * app_version : 1.0
             * createc_at : 2018-10-04 10:21:25.0
             * updated_at : 2018-10-04 10:21:25.0
             */

            private int id;
            private String os_type;
            private String device_token;
            private String device_model;
            private String os_version;
            private String app_version;
            private String createc_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getOs_type() {
                return os_type;
            }

            public void setOs_type(String os_type) {
                this.os_type = os_type;
            }

            public String getDevice_token() {
                return device_token;
            }

            public void setDevice_token(String device_token) {
                this.device_token = device_token;
            }

            public String getDevice_model() {
                return device_model;
            }

            public void setDevice_model(String device_model) {
                this.device_model = device_model;
            }

            public String getOs_version() {
                return os_version;
            }

            public void setOs_version(String os_version) {
                this.os_version = os_version;
            }

            public String getApp_version() {
                return app_version;
            }

            public void setApp_version(String app_version) {
                this.app_version = app_version;
            }

            public String getCreatec_at() {
                return createc_at;
            }

            public void setCreatec_at(String createc_at) {
                this.createc_at = createc_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class RolesBean {
            /**
             * id : 1
             * role : ROLE_ADMIN
             */

            private int id;
            private String role;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getRole() {
                return role;
            }

            public void setRole(String role) {
                this.role = role;
            }
        }
    }
}
