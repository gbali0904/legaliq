package com.legaliq.android.app.setting.changePassword;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.dd.ShadowLayout;
import com.legaliq.android.app.MainActivity;
import com.legaliq.android.app.R;
import com.legaliq.android.app.application.AppController;
import com.legaliq.android.app.login.LoginActivity;
import com.legaliq.android.app.login.model.ModelForLogin;
import com.legaliq.android.app.setting.changePassword.model.ModelForChangePassword;
import com.legaliq.android.app.setting.changePassword.presenter.ChangePasswordPresenterCompl;
import com.legaliq.android.app.setting.changePassword.view.ChangePasswordView;
import com.legaliq.android.app.utility.DialogUtils;
import com.legaliq.android.app.utility.TAGs;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePassword extends Fragment implements ChangePasswordView {
    public static final String TAG = ChangePassword.class.getSimpleName();
    @BindView(R.id.edtNewPass)
    EditText edtNewPass;
    @BindView(R.id.edtpassword)
    EditText edtpassword;
    @BindView(R.id.btnChangeFirstPassword)
    Button btnChangeFirstPassword;
    @BindView(R.id.edtOldPass)
    EditText edtOldPass;
    @BindView(R.id.btnChangePassword)
    Button btnChangePassword;
    @BindView(R.id.oldLay)
    TextInputLayout oldLay;
    @BindView(R.id.changeFirstShadow)
    ShadowLayout changeFirstShadow;
    @BindView(R.id.chnagePasswordShadow)
    ShadowLayout changePasswordShadow;
    private View rootView;
    private String newPass;
    private String password;
    private ChangePasswordPresenterCompl changePasswordPresenter;
    private String user_id;
    private boolean passwordStatus;
    private String oldPass;
    private ModelForLogin modelForLogin;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "onDetach");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_change_password, container, false);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, rootView);


        changePasswordPresenter = new ChangePasswordPresenterCompl(this);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            passwordStatus = bundle.getBoolean(TAGs.PASSWORD_STATUS, false);
        }

        if (passwordStatus) {
            changeFirstShadow.setVisibility(View.VISIBLE);
            oldLay.setVisibility(View.GONE);
            changePasswordShadow.setVisibility(View.GONE);

        } else {
            oldLay.setVisibility(View.VISIBLE);
            changePasswordShadow.setVisibility(View.VISIBLE);
            changeFirstShadow.setVisibility(View.GONE);
        }

        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void changeFirstTimeLoginPasswordServerError(String message) {
        AppController.getInstance().hideProgressDialog();
        DialogUtils.showDialog(getActivity(), message);
    }

    @Override
    public void changeFirstTimeLoginPasswordSuccess(ModelForChangePassword modelForChangePassword) {
        AppController.getInstance().hideProgressDialog();
        getActivity().finish();
    }

    @Override
    public void onSetProgressBarVisibility(int visiblity) {
        AppController.getInstance().showProgressDialog(getActivity());
    }

    @Override
    public void changePasswordServerError(String message) {
        AppController.getInstance().hideProgressDialog();
        DialogUtils.showDialog(getActivity(), message);
    }

    @Override
    public void changePasswordSuccess(ModelForLogin modelForLogin) {
        AppController.getInstance().hideProgressDialog();

    }


    @OnClick({R.id.btnChangeFirstPassword, R.id.btnChangePassword})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnChangeFirstPassword:
                newPass = edtNewPass.getText().toString().trim();
                password = edtpassword.getText().toString().trim();
                if (TextUtils.isEmpty(newPass)) {
                    edtNewPass.setError(getResources().getString(R.string.error_mandatory));
                    return;
                }
                if (!AppController.getInstance().passwordValidator(newPass)) {
                    edtNewPass.setError(getResources().getString(R.string.validatePassword));
                    return;
                }
                if (TextUtils.isEmpty(password)) {
                    edtpassword.setError(getResources().getString(R.string.error_mandatory));
                    return;
                }
                if (!newPass.equals(password)) {
                    edtpassword.setError(getResources().getString(R.string.passnotmatch));
                    return;
                } else {
                    changePasswordPresenter.setProgressBarVisiblity(0);
                    changePasswordPresenter.doChangeFirstTimeLoginPassword( password);
                }

                break;
            case R.id.btnChangePassword:

                oldPass = edtOldPass.getText().toString().trim();
                newPass = edtNewPass.getText().toString().trim();
                password = edtpassword.getText().toString().trim();
                if (TextUtils.isEmpty(oldPass)) {
                    edtOldPass.setError(getResources().getString(R.string.error_mandatory));
                    return;
                }
                if (TextUtils.isEmpty(newPass)) {
                    edtNewPass.setError(getResources().getString(R.string.error_mandatory));
                    return;
                }
                if (!AppController.getInstance().passwordValidator(newPass)) {
                    edtNewPass.setError(getResources().getString(R.string.validatePassword));
                    return;
                }
                if (TextUtils.isEmpty(password)) {
                    edtpassword.setError(getResources().getString(R.string.error_mandatory));
                    return;
                }
                if (!newPass.equals(password)) {
                    edtpassword.setError(getResources().getString(R.string.passnotmatch));
                    return;
                } else {
                    //changePasswordPresenter.setProgressBarVisiblity(0);
                    //changePasswordPresenter.doChangePassword(user_id, oldPass, password);
                }

                break;
        }
    }


}
