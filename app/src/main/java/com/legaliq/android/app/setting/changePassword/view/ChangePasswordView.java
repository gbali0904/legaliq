package com.legaliq.android.app.setting.changePassword.view;


import com.legaliq.android.app.login.model.ModelForLogin;
import com.legaliq.android.app.setting.changePassword.model.ModelForChangePassword;

public interface ChangePasswordView {
    void changeFirstTimeLoginPasswordServerError(String message);

    void changeFirstTimeLoginPasswordSuccess(ModelForChangePassword modelForChangePassword);

    void onSetProgressBarVisibility(int visiblity);

    void changePasswordServerError(String message);

    void changePasswordSuccess(ModelForLogin modelForChangePassword);

}
