package com.legaliq.android.app.utility.dialog;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.DatePicker;


import com.legaliq.android.app.application.AppController;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Android on 8/16/2017.
 */

public class SelectDateFormat extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private static final int DATE_DIALOG_ID = 1;
    private static OnDatePickedListner onDatePickedListner;
    private static SelectDateFormat mInstance;
    private static Date date;

    public static SelectDateFormat getInstance(Date d) {
        if (mInstance == null)
            mInstance = new SelectDateFormat();
        date=d;
        return mInstance;
    }
    /* public SelectDateFormat(Date d) {
           date=d;
       }*/
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
       /* Date d=new Date();
        Calendar cal=Calendar.getInstance();
        cal.set(2000, 11, 31, 0, 0);
        d.setTime(cal.getTimeInMillis());*/
        String date = AppController.getInstance().getPreference().getString("date", "");
        Calendar minCal = Calendar.getInstance();
        minCal.set(1950, 1, 1, 0, 0, 0);
        Log.e("this",date);
        Calendar c = Calendar.getInstance();
        int cyear = c.get(Calendar.YEAR);
        int cmonth = c.get(Calendar.MONTH);
        int cday = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(getActivity(),
                this, cyear, cmonth, cday);
        //dialog.getDatePicker().setMaxDate(this.date.getTime());
       // dialog.getDatePicker().setMinDate(minCal.getTimeInMillis());
        return dialog;
    }
    public void onDateSet(DatePicker view, int yy, int mm, int dd) {
        populateSetDate(yy, mm+1, dd);
    }
    public void populateSetDate(int year, int month, int day) {
        if(onDatePickedListner!=null){
            onDatePickedListner.onDatePicked(year,month,day);
        }
    }
    public static void setOnDatePickedListener(OnDatePickedListner listener){
        onDatePickedListner = listener;
    }
    public interface OnDatePickedListner{
        void onDatePicked(int year, int month, int day);
    }
}