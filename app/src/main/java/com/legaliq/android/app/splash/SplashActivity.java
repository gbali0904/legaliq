package com.legaliq.android.app.splash;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.legaliq.android.app.R;
import com.legaliq.android.app.application.AppController;
import com.legaliq.android.app.login.LoginActivity;
import com.legaliq.android.app.utility.Constants;
import com.legaliq.android.app.utility.TAGs;

import java.util.Timer;
import java.util.TimerTask;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SplashActivity extends AppCompatActivity {

    private static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 0;
    private static int Splash_TIME_DELEY = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.READ_PHONE_STATE},100);
        }

        if (AppController.getInstance().getPreference().getString(Constants.GCM_ID, "").equals("")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE},
                            PERMISSIONS_REQUEST_READ_PHONE_STATE);
                }
            }
            getALLContent();
        }
        else {
            nextLaunch();
        }
    }


    private void getALLContent() {
        //OS Version
        String CVersion = Build.VERSION.RELEASE;
        AppController.getInstance().getPreferenceEditor().putString(Constants.OS_VERSION,CVersion).commit();

        //Model Name
        String model = Build.MODEL;
        AppController.getInstance().getPreferenceEditor().putString(Constants.DEVICE_MODEL,model).commit();

        //Build Version
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            AppController.getInstance().getPreferenceEditor().putString(Constants.VERSION_NAME,version).commit();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        AppController.getInstance().getPreferenceEditor().putString(Constants.GCM_ID, refreshedToken).commit();
        Log.e("this","key "+refreshedToken);
        nextLaunch();
    }

    private void nextLaunch() {
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if(AppController.getInstance().getPreference().getBoolean(TAGs.LOGIN_STATUS,false)){

                }
                else {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };
        Timer t = new Timer();
        t.schedule(task, Splash_TIME_DELEY);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
