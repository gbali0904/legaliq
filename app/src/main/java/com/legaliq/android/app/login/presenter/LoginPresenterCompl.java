package com.legaliq.android.app.login.presenter;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import com.legaliq.android.app.application.AppController;
import com.legaliq.android.app.login.model.ModelForForgotPassword;
import com.legaliq.android.app.login.model.ModelForLogin;
import com.legaliq.android.app.login.view.LoginView;
import com.legaliq.android.app.utility.Constants;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by gunjan on 2018/09/10.
 */
public class LoginPresenterCompl implements LoginPresenter {

	LoginView loginView;
	Handler handler;
	private Subscription subscription;
	public LoginPresenterCompl(LoginView loginView) {
		this.loginView = loginView;
		handler = new Handler(Looper.getMainLooper());
	}

	@Override
	public void doLogin(String userId, final String password) {
		String refreshedToken = FirebaseInstanceId.getInstance().getToken();
		AppController.getInstance().getPreferenceEditor().putString(Constants.GCM_ID, refreshedToken).commit();
		String deviceToken = AppController.getInstance().getPreference().getString(Constants.GCM_ID, "");
		String appVersion =AppController.getInstance().getPreference().getString(Constants.VERSION_NAME,"");
		String deviceModel = AppController.getInstance().getPreference().getString(Constants.DEVICE_MODEL, "");
		String osVersion = AppController.getInstance().getPreference().getString(Constants.OS_VERSION, "");
		String osType = Constants.DEVICE_TYPE;

		subscription = AppController.getInstance().getRetrofitServices().login(userId,password,
				deviceToken,deviceModel,
				osType,osVersion,appVersion)
				.observeOn(AndroidSchedulers.mainThread())
				.subscribeOn(AppController.getInstance().defaultSubscribeScheduler())
				.subscribe(new Subscriber<ModelForLogin>() {
					@Override
					public void onCompleted() {
					}
					@Override
					public void onError(Throwable e) {
						Log.e("this", "signIn Error:" + e.getMessage());
						loginView.loginServerError(e.getMessage());
						e.printStackTrace();
					}
					@Override
					public void onNext(ModelForLogin modelForLogin) {
						loginView.loginSuccess(modelForLogin);
					}
				});

	}

	@Override
	public void setProgressBarVisiblity(int visiblity){
		loginView.onSetProgressBarVisibility(visiblity);
	}

	@Override
	public void doforgotpassword(String mobile_number) {
		subscription = AppController.getInstance().getRetrofitServices().forgotPassword(mobile_number)
				.observeOn(AndroidSchedulers.mainThread())
				.subscribeOn(AppController.getInstance().defaultSubscribeScheduler())
				.subscribe(new Subscriber<ModelForForgotPassword>() {
					@Override
					public void onCompleted() {
					}
					@Override
					public void onError(Throwable e) {
						Log.e("this", "signIn Error:" + e.getMessage());
						loginView.loginServerError(e.getMessage());
						e.printStackTrace();
					}
					@Override
					public void onNext(ModelForForgotPassword modelForForgotPassword) {
						loginView.forgotPasswordSuccess(modelForForgotPassword);
					}
				});
	}


}
