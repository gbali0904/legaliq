package com.legaliq.android.app.utility;

/**
 * Created by apple on 08/07/16.
 */
public class TAGs {

    public static final String PASSWORD_STATUS = "password_status";
    public static final String JSON_LOGIN_DATA = "json_login_data";
    public static final String PASSWORD = "password";
    public static final String HEADER_BASIC_AUTHENTICATION = "header_basic_auth";
    public static final String LOGIN_STATUS = "login_status";
}
