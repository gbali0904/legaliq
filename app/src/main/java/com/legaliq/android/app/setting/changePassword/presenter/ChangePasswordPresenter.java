package com.legaliq.android.app.setting.changePassword.presenter;

public interface ChangePasswordPresenter {

    void doChangeFirstTimeLoginPassword(String passwd);
    void setProgressBarVisiblity(int visiblity);

    void doChangePassword(String user_id, String oldPass, String password);

}
