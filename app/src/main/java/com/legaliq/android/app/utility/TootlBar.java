package com.legaliq.android.app.utility;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class TootlBar {
    public static void setToolBar(final AppCompatActivity appCompatTextView, Toolbar toolbar) {
        toolbar.setTitle("");
        appCompatTextView.setSupportActionBar(toolbar);
        appCompatTextView.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        appCompatTextView.getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appCompatTextView.onBackPressed();
            }
        });
    }

}
