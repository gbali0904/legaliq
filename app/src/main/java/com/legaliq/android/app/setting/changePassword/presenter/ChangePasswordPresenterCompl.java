package com.legaliq.android.app.setting.changePassword.presenter;

import android.util.Log;

import com.legaliq.android.app.application.AppController;
import com.legaliq.android.app.login.model.ModelForLogin;
import com.legaliq.android.app.setting.changePassword.model.ModelForChangePassword;
import com.legaliq.android.app.setting.changePassword.view.ChangePasswordView;
import com.legaliq.android.app.utility.TAGs;

import java.util.HashMap;
import java.util.Map;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class ChangePasswordPresenterCompl implements ChangePasswordPresenter {

    private final ChangePasswordView changePasswordView;
    private Subscription subscription;

    public ChangePasswordPresenterCompl(ChangePasswordView changePasswordView){
        this.changePasswordView=changePasswordView;
    }
    @Override
    public void doChangeFirstTimeLoginPassword(String password) {
        subscription = AppController.getInstance().getRetrofitServices().changeFirstTimeLoginPassword(
                AppController.getInstance().getPreference().getString(TAGs.HEADER_BASIC_AUTHENTICATION, ""),password)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AppController.getInstance().defaultSubscribeScheduler())
                .subscribe(new Subscriber<ModelForChangePassword>() {
                    @Override
                    public void onCompleted() {
                    }
                    @Override
                    public void onError(Throwable e) {
                        Log.e("this", "signIn Error:" + e.getMessage());
                        changePasswordView.changeFirstTimeLoginPasswordServerError(e.getMessage());
                        e.printStackTrace();
                    }
                    @Override
                    public void onNext(ModelForChangePassword modelForChangePassword) {
                        changePasswordView.changeFirstTimeLoginPasswordSuccess(modelForChangePassword);
                    }
                });
    }

    @Override
    public void setProgressBarVisiblity(int visiblity) {
        changePasswordView.onSetProgressBarVisibility(visiblity);
    }

    @Override
    public void doChangePassword(String user_id, String oldPass, String password) {
        subscription = AppController.getInstance().getRetrofitServices().changePassword(user_id,oldPass,password)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AppController.getInstance().defaultSubscribeScheduler())
                .subscribe(new Subscriber<ModelForLogin>() {
                    @Override
                    public void onCompleted() {
                    }
                    @Override
                    public void onError(Throwable e) {
                        Log.e("this", "signIn Error:" + e.getMessage());
                        changePasswordView.changePasswordServerError(e.getMessage());
                        e.printStackTrace();
                    }
                    @Override
                    public void onNext(ModelForLogin modelForLogin) {
                        changePasswordView.changePasswordSuccess(modelForLogin);
                    }
                });
    }
}
