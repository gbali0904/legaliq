package com.legaliq.android.app.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.FirebaseException;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.legaliq.android.app.MainActivity;
import com.legaliq.android.app.R;
import com.legaliq.android.app.application.AppController;
import com.legaliq.android.app.login.model.ModelForForgotPassword;
import com.legaliq.android.app.login.model.ModelForLogin;
import com.legaliq.android.app.login.presenter.LoginPresenterCompl;
import com.legaliq.android.app.login.view.LoginView;
import com.legaliq.android.app.setting.changePassword.ChangePassword;
import com.legaliq.android.app.utility.DialogUtils;
import com.legaliq.android.app.utility.FragmentManagerUtil;
import com.legaliq.android.app.utility.TAGs;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by gunjan on 2018/09/10.
 */
public class LoginActivity extends AppCompatActivity implements LoginView  {

    private static final String TAG = LoginActivity.class.getSimpleName();
    @BindView(R.id.edtLoginEmail)
    EditText edtLoginEmail;
    @BindView(R.id.edtLoginpassword)
    EditText edtLoginpassword;
    @BindView(R.id.btnforLogin)
    Button btnLogin;
    @BindView(R.id.input_layout_number)
    TextInputLayout inputLayoutNumber;
    @BindView(R.id.input_layout_password)
    TextInputLayout inputLayoutPassword;
    @BindView(R.id.content_main)
    RelativeLayout contentMain;
    @BindView(R.id.forgotPassword)
    TextView forgotPassword;
    private String loginId;
    private String password;
    private LoginPresenterCompl loginPresenter;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        loginPresenter = new LoginPresenterCompl(this);
    }

    @Override
    public void onSetProgressBarVisibility(int visibility) {
        AppController.getInstance().showProgressDialog(this);
    }

    @Override
    public void loginSuccess(ModelForLogin modelForLogin) {
        AppController.getInstance().hideProgressDialog();

        if (modelForLogin.isStatus())
        {
            String user_login_jsonData = AppController.getInstance().getGson().toJson(modelForLogin).toString();
            AppController.getInstance().getPreferenceEditor().putString(TAGs.JSON_LOGIN_DATA, user_login_jsonData).commit();
            AppController.getInstance().getPreferenceEditor().putString(TAGs.PASSWORD,password).commit();
            if ( modelForLogin.getUsers().getFirst_login() == 0)
            {
                String basic = authenticationData();
                AppController.getInstance().getPreferenceEditor().putString(TAGs.HEADER_BASIC_AUTHENTICATION, basic).commit();
                ChangePassword changePassword=new ChangePassword();
                Bundle bundle = new Bundle();
                bundle.putBoolean(TAGs.PASSWORD_STATUS, true);
                changePassword.setArguments(bundle);
                FragmentManagerUtil.replaceFragment(getSupportFragmentManager(), R.id.content_main,
                        changePassword, true, ChangePassword.TAG);
            }
            else {
                AppController.getInstance().getPreferenceEditor().putBoolean(TAGs.LOGIN_STATUS, true).commit();

            }
        }
        else {
            DialogUtils.showDialog(LoginActivity.this, modelForLogin.getMessage());
        }
    }

    @Override
    public void loginServerError(String message) {
        AppController.getInstance().hideProgressDialog();
        DialogUtils.showDialog(LoginActivity.this, message);
    }

    @Override
    public void forgotPasswordSuccess(ModelForForgotPassword modelForForgotPassword) {
        AppController.getInstance().hideProgressDialog();
    }
    @NonNull
    private String authenticationData() {
        String json_string = AppController.getInstance().getPreference().getString(TAGs.JSON_LOGIN_DATA, "");
        ModelForLogin dataForLogin = AppController.getInstance().getGson().fromJson(json_string, ModelForLogin.class);
        String password= AppController.getInstance().getPreference().getString(TAGs.PASSWORD, "");
        ModelForLogin.UsersBean response = dataForLogin.getUsers();
        String credentials = response.getMobile_no() + ":" + password;
        return "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
    }

    @OnClick({R.id.btnforLogin, R.id.forgotPassword})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnforLogin:
                loginValidation();
                break;
            case R.id.forgotPassword:
                forgotPassword();
                break;
        }
    }

    private void loginValidation() {
        loginId = edtLoginEmail.getText().toString().trim();
        password = edtLoginpassword.getText().toString().trim();
        if (TextUtils.isEmpty(loginId)) {
            edtLoginEmail.setError(getResources().getString(R.string.error_mandatory));
            return;
        }
        if (TextUtils.isEmpty(password)) {
            edtLoginpassword.setError(getResources().getString(R.string.error_mandatory));
            return;
        }
        if (!AppController.getInstance().passwordValidator(password)) {
            edtLoginpassword.setError(getResources().getString(R.string.validatePassword));
            return;
        } else {
            loginPresenter.setProgressBarVisiblity(0);
            loginPresenter.doLogin(loginId, password);
        }
    }

    private void forgotPassword() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_forgot_password, null);
        dialogBuilder.setView(dialogView);
        final EditText edtfornumber = (EditText) dialogView.findViewById(R.id.edtforfpnumber);
        Button forgotpassword = (Button) dialogView.findViewById(R.id.btnforforgotpassword);
        forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mobile_number = edtfornumber.getText().toString();
                if (TextUtils.isEmpty(mobile_number)) {
                    edtfornumber.setError(getResources().getString(R.string.error_mandatory));
                    return;
                }
                else {
                    // loginPresenter.setProgressBarVisiblity(0);
                    // loginPresenter.doforgotpassword(mobile_number);

                }
            }
        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
